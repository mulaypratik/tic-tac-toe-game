VERSION 5.00
Begin VB.Form Form2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "1P vs Computer"
   ClientHeight    =   7860
   ClientLeft      =   2760
   ClientTop       =   1785
   ClientWidth     =   9405
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7860
   ScaleWidth      =   9405
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer T9 
      Enabled         =   0   'False
      Interval        =   900
      Left            =   0
      Top             =   2880
   End
   Begin VB.Timer T8 
      Enabled         =   0   'False
      Interval        =   900
      Left            =   0
      Top             =   2520
   End
   Begin VB.Timer T7 
      Enabled         =   0   'False
      Interval        =   900
      Left            =   0
      Top             =   2160
   End
   Begin VB.Timer T6 
      Enabled         =   0   'False
      Interval        =   900
      Left            =   0
      Top             =   1800
   End
   Begin VB.Timer T5 
      Enabled         =   0   'False
      Interval        =   900
      Left            =   0
      Top             =   1440
   End
   Begin VB.Timer T4 
      Enabled         =   0   'False
      Interval        =   900
      Left            =   0
      Top             =   1080
   End
   Begin VB.Timer T3 
      Enabled         =   0   'False
      Interval        =   900
      Left            =   0
      Top             =   720
   End
   Begin VB.Timer T2 
      Enabled         =   0   'False
      Interval        =   900
      Left            =   0
      Top             =   360
   End
   Begin VB.Timer T1 
      Enabled         =   0   'False
      Interval        =   900
      Left            =   0
      Top             =   0
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   6360
      TabIndex        =   21
      Top             =   5880
      Width           =   1695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4200
      TabIndex        =   1
      Top             =   960
      Width           =   1215
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4200
      TabIndex        =   0
      Top             =   240
      Width           =   3015
   End
   Begin VB.OptionButton o 
      Caption         =   "  O"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   375
      Left            =   4680
      TabIndex        =   13
      Top             =   2400
      Width           =   975
   End
   Begin VB.OptionButton x 
      Caption         =   "  X"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   375
      Left            =   4680
      TabIndex        =   12
      Top             =   1800
      Width           =   975
   End
   Begin VB.CommandButton exit 
      Caption         =   "Quit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   6360
      TabIndex        =   11
      Top             =   6840
      Width           =   1695
   End
   Begin VB.CommandButton I 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3360
      TabIndex        =   10
      Top             =   5760
      Width           =   1095
   End
   Begin VB.CommandButton H 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   2040
      TabIndex        =   9
      Top             =   5760
      Width           =   1095
   End
   Begin VB.CommandButton G 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   720
      TabIndex        =   8
      Top             =   5760
      Width           =   1095
   End
   Begin VB.CommandButton F 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3360
      TabIndex        =   7
      Top             =   4680
      Width           =   1095
   End
   Begin VB.CommandButton E 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   2040
      TabIndex        =   6
      Top             =   4680
      Width           =   1095
   End
   Begin VB.CommandButton D 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   720
      TabIndex        =   5
      Top             =   4680
      Width           =   1095
   End
   Begin VB.CommandButton C 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3360
      TabIndex        =   4
      Top             =   3600
      Width           =   1095
   End
   Begin VB.CommandButton B 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   2040
      TabIndex        =   3
      Top             =   3600
      Width           =   1095
   End
   Begin VB.CommandButton A 
      BackColor       =   &H00808080&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   720
      TabIndex        =   2
      Top             =   3600
      Width           =   1095
   End
   Begin VB.Label Label3 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "MS Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000040C0&
      Height          =   495
      Left            =   5880
      TabIndex        =   22
      Top             =   2280
      Width           =   3255
   End
   Begin VB.Label Label5 
      Caption         =   "Score board"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   375
      Left            =   5760
      TabIndex        =   20
      Top             =   3240
      Width           =   3015
   End
   Begin VB.Label Label6 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   5160
      TabIndex        =   19
      Top             =   4080
      Width           =   1815
   End
   Begin VB.Label Label7 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF8080&
      Height          =   375
      Left            =   7440
      TabIndex        =   18
      Top             =   4080
      Width           =   1815
   End
   Begin VB.Line Line13 
      BorderWidth     =   3
      X1              =   7200
      X2              =   7200
      Y1              =   3960
      Y2              =   5400
   End
   Begin VB.Label Label8 
      BackColor       =   &H00C0E0FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   5760
      TabIndex        =   17
      Top             =   4920
      Width           =   1095
   End
   Begin VB.Label Label9 
      BackColor       =   &H00C0E0FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   7800
      TabIndex        =   16
      Top             =   4920
      Width           =   1095
   End
   Begin VB.Line Line14 
      BorderWidth     =   3
      X1              =   5160
      X2              =   9240
      Y1              =   4680
      Y2              =   4680
   End
   Begin VB.Label Label2 
      Caption         =   "Enter  your name :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   375
      Left            =   1560
      TabIndex        =   15
      Top             =   360
      Width           =   2295
   End
   Begin VB.Label Label1 
      Caption         =   "Select your symbol :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   495
      Left            =   1320
      TabIndex        =   14
      Top             =   1800
      Width           =   4335
   End
   Begin VB.Line Line12 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   4680
      X2              =   480
      Y1              =   3360
      Y2              =   6840
   End
   Begin VB.Line Line11 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   480
      X2              =   4680
      Y1              =   3360
      Y2              =   6840
   End
   Begin VB.Line Line10 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   2520
      X2              =   2520
      Y1              =   3120
      Y2              =   7080
   End
   Begin VB.Line Line9 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   240
      X2              =   4920
      Y1              =   5040
      Y2              =   5040
   End
   Begin VB.Line Line8 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   1320
      X2              =   1320
      Y1              =   3120
      Y2              =   7080
   End
   Begin VB.Line Line7 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   240
      X2              =   4920
      Y1              =   6240
      Y2              =   6240
   End
   Begin VB.Line Line6 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   3840
      X2              =   3840
      Y1              =   3120
      Y2              =   7080
   End
   Begin VB.Line Line5 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   240
      X2              =   4920
      Y1              =   3960
      Y2              =   3960
   End
   Begin VB.Line Line4 
      BorderWidth     =   5
      X1              =   360
      X2              =   4920
      Y1              =   5640
      Y2              =   5640
   End
   Begin VB.Line Line3 
      BorderWidth     =   5
      X1              =   360
      X2              =   4920
      Y1              =   4560
      Y2              =   4560
   End
   Begin VB.Line Line2 
      BorderWidth     =   5
      X1              =   3240
      X2              =   3240
      Y1              =   3120
      Y2              =   7080
   End
   Begin VB.Line Line1 
      BorderWidth     =   5
      X1              =   1920
      X2              =   1920
      Y1              =   3120
      Y2              =   7080
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim s1 As String, s2 As String, n1 As String, m1 As Integer, m2 As Integer, ac As Integer, bc As Integer, cc As Integer, dc As Integer, ec As Integer, fc As Integer, gc As Integer, hc As Integer, ic As Integer
Private Sub A_Click()
A.Caption = s1
A.Enabled = False
Label3.Caption = "Computer's turn."
Form2.Enabled = False
ac = ac + 1
If ac = 9 Then
ac = 0
End If
starttime = Now
T1.Enabled = True
End Sub

Private Sub B_Click()
B.Caption = s1
B.Enabled = False
Label3.Caption = "Computer's turn."
Form2.Enabled = False
bc = bc + 1
If bc = 9 Then
bc = 0
End If
starttime = Now
T2.Enabled = True
End Sub

Private Sub C_Click()
C.Caption = s1
C.Enabled = False
Label3.Caption = "Computer's turn."
Form2.Enabled = False
cc = cc + 1
If cc = 9 Then
cc = 0
End If
starttime = Now
T3.Enabled = True
End Sub

Private Sub Command2_Click()
m1 = 0
m2 = 0
Label8.Caption = m1
Label9.Caption = m2
End Sub

Private Sub Command1_Click()
If Text1.Text = "" Then
Text1.Text = "Player1"
End If
x.Enabled = True
o.Enabled = True
Text1.Enabled = False
Label6.Caption = Text1.Text
Label7.Caption = "Computer"
Label8.Caption = m1
Label9.Caption = m2
Command2.Enabled = True
Command1.Enabled = False
End Sub

Private Sub D_Click()
D.Caption = s1
D.Enabled = False
Label3.Caption = "Computer's turn."
Form2.Enabled = False
dc = dc + 1
If dc = 9 Then
dc = 0
End If
starttime = Now
T4.Enabled = True
End Sub

Private Sub E_Click()
E.Caption = s1
E.Enabled = False
Label3.Caption = "Computer's turn."
Form2.Enabled = False
ec = ec + 1
If ec = 9 Then
ec = 0
End If
starttime = Now
T5.Enabled = True
End Sub

Private Sub exit_Click()
m1 = 0
m2 = 0
ac = 0
bc = 0
cc = 0
dc = 0
ec = 0
fc = 0
gc = 0
hc = 0
ic = 0
A.Caption = ""
B.Caption = ""
C.Caption = ""
D.Caption = ""
E.Caption = ""
F.Caption = ""
G.Caption = ""
H.Caption = ""
I.Caption = ""
A.Enabled = False
B.Enabled = False
C.Enabled = False
D.Enabled = False
E.Enabled = False
F.Enabled = False
G.Enabled = False
H.Enabled = False
I.Enabled = False
o.Enabled = True
x.Enabled = True
o.Enabled = True
Label3.Caption = ""
Label6.Caption = ""
Label7.Caption = ""
Label8.Caption = ""
Label9.Caption = ""
Text1.Text = ""
Text1.Enabled = True
x.Enabled = False
o.Enabled = False
Command1.Enabled = True
Command2.Enabled = False
Form2.Hide
Form1.Show
End Sub

Private Sub F_Click()
F.Caption = s1
F.Enabled = False
Label3.Caption = "Computer's turn."
Form2.Enabled = False
fc = fc + 1
If fc = 9 Then
fc = 0
End If
starttime = Now
T6.Enabled = True
End Sub

Private Sub Form_Load()
m1 = 0
m2 = 0
ac = 0
bc = 0
cc = 0
dc = 0
fc = 0
gc = 0
hc = 0
ic = 0
A.Enabled = False
B.Enabled = False
C.Enabled = False
D.Enabled = False
E.Enabled = False
F.Enabled = False
G.Enabled = False
H.Enabled = False
I.Enabled = False
x.Enabled = False
o.Enabled = False
End Sub

Private Sub G_Click()
G.Caption = s1
G.Enabled = False
Label3.Caption = "Computer's turn."
Form2.Enabled = False
gc = gc + 1
If gc = 9 Then
gc = 0
End If
starttime = Now
T7.Enabled = True
End Sub

Private Sub H_Click()
H.Caption = s1
H.Enabled = False
Label3.Caption = "Computer's turn."
Form2.Enabled = False
hc = hc + 1
If hc = 9 Then
hc = 0
End If
starttime = Now
T8.Enabled = True
End Sub

Private Sub I_Click()
I.Caption = s1
I.Enabled = False
Label3.Caption = "Computer's turn."
Form2.Enabled = False
ic = ic + 1
If ic = 9 Then
ic = 0
End If
starttime = Now
T9.Enabled = True
End Sub


Private Sub T1_Timer()
Form2.Enabled = True
If (A.Caption = B.Caption) And (B.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (A.Caption = B.Caption) And (B.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (ac = 0) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (ac = 1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (ac = 2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (ac = 3) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (ac = 4) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (ac = 5) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (ac = 6) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (ac = 7) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (ac = 8) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (C.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s1) Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s1) Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s1) Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s1) Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s1) Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s1) Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Win!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s1) Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Win!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s1) Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s2) Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s2) Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s2) Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s2) Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s2) Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s2) Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s2) Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s2) Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If
Label3.Caption = n1 + "'s turn."
T1.Enabled = False
End Sub

Private Sub T2_Timer()
Form2.Enabled = True
If (A.Caption = B.Caption) And (B.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (A.Caption = B.Caption) And (B.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (bc = 0) And (C.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (bc = 1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (bc = 2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (bc = 3) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (bc = 4) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (bc = 5) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (bc = 6) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (bc = 7) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (bc = 8) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s1) Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s1) Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s1) Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s1) Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s1) Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s1) Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s1) Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s1) Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s2) Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s2) Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s2) Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s2) Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s2) Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s2) Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s2) Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s2) Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Win!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If
Label3.Caption = n1 + "'s turn."
T2.Enabled = False
End Sub

Private Sub T3_Timer()
Form2.Enabled = True
If (A.Caption = B.Caption) And (B.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (A.Caption = B.Caption) And (B.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (cc = 0) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (cc = 1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (cc = 2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (cc = 3) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (cc = 4) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (cc = 5) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (cc = 6) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (cc = 7) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (cc = 8) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s1) Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s1) Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s1) Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s1) Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s1) Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s1) Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s1) Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s1) Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s2) Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s2) Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s2) Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s2) Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s2) Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s2) Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s2) Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s2) Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If
Label3.Caption = n1 + "'s turn."
T3.Enabled = False
End Sub

Private Sub T4_Timer()
Form2.Enabled = True
If (A.Caption = B.Caption) And (B.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (A.Caption = B.Caption) And (B.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (dc = 0) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (dc = 1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (dc = 2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (dc = 3) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (dc = 4) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (dc = 5) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (dc = 6) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (dc = 7) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (dc = 8) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s1) Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s1) Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s1) Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s1) Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s1) Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s1) Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s1) Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s1) Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s2) Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s2) Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s2) Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s2) Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s2) Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s2) Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s2) Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s2) Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If
Label3.Caption = n1 + "'s turn."
T4.Enabled = False
End Sub

Private Sub T5_Timer()
Form2.Enabled = True
If (A.Caption = B.Caption) And (B.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (A.Caption = B.Caption) And (B.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (ec = 0) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (ec = 1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (ec = 2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (ec = 3) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (ec = 4) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (ec = 5) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (ec = 6) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (ec = 7) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (ec = 8) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s1) Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s1) Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s1) Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s1) Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s1) Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s1) Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s1) Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s1) Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s2) Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s2) Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s2) Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s2) Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s2) Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s2) Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s2) Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s2) Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If
Label3.Caption = n1 + "'s turn."
T5.Enabled = False
End Sub

Private Sub T6_Timer()
Form2.Enabled = True
If (A.Caption = B.Caption) And (B.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (A.Caption = B.Caption) And (B.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (fc = 0) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (fc = 1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (fc = 2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (fc = 3) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (fc = 4) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (fc = 5) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (fc = 6) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (fc = 7) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (fc = 8) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s1) Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s1) Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s1) Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s1) Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s1) Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s1) Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s1) Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s1) Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s2) Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s2) Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s2) Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s2) Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s2) Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s2) Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s2) Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s2) Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If
Label3.Caption = n1 + "'s turn."
T6.Enabled = False
End Sub

Private Sub T7_Timer()
Form2.Enabled = True
If (A.Caption = B.Caption) And (B.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (A.Caption = B.Caption) And (B.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (gc = 0) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (gc = 1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (gc = 2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (gc = 3) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (gc = 4) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (gc = 5) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (gc = 6) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (gc = 7) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (gc = 8) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s1) Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s1) Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s1) Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s1) Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s1) Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s1) Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s1) Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s1) Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s2) Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s2) Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s2) Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s2) Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s2) Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s2) Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s2) Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s2) Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If
Label3.Caption = n1 + "'s turn."
T7.Enabled = False
End Sub

Private Sub T8_Timer()
Form2.Enabled = True
If (A.Caption = B.Caption) And (B.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (A.Caption = B.Caption) And (B.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (hc = 0) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (hc = 1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (hc = 2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (hc = 3) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (hc = 4) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (hc = 5) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (hc = 6) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (hc = 7) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (hc = 8) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s1) Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s1) Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s1) Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s1) Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s1) Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s1) Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s1) Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s1) Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s2) Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s2) Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s2) Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s2) Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s2) Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s2) Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s2) Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s2) Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If
Label3.Caption = n1 + "'s turn."
T8.Enabled = False
End Sub

Private Sub T9_Timer()
Form2.Enabled = True
If (A.Caption = B.Caption) And (B.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s2) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s2) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s2) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s2) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s2) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s2) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s2) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s2) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (A.Caption = B.Caption) And (B.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Caption = C.Caption) And (C.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = C.Caption) And (C.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Caption = E.Caption) And (E.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (E.Caption = F.Caption) And (F.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (D.Caption = F.Caption) And (F.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = H.Caption) And (H.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (H.Caption = I.Caption) And (I.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = I.Caption) And (I.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (G.Caption = D.Caption) And (D.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (D.Caption = A.Caption) And (A.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = A.Caption) And (A.Caption = s1) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (H.Caption = E.Caption) And (E.Caption = s1) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (E.Caption = B.Caption) And (B.Caption = s1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (H.Caption = B.Caption) And (B.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (I.Caption = F.Caption) And (F.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (F.Caption = C.Caption) And (C.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (I.Caption = C.Caption) And (C.Caption = s1) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (A.Caption = E.Caption) And (E.Caption = s1) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (E.Caption = I.Caption) And (I.Caption = s1) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (A.Caption = I.Caption) And (I.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (G.Caption = E.Caption) And (E.Caption = s1) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (E.Caption = C.Caption) And (C.Caption = s1) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (G.Caption = C.Caption) And (C.Caption = s1) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (ic = 0) And (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (ic = 1) And (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (ic = 2) And (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (ic = 3) And (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (ic = 4) And (I.Enabled = True) Then
I.Caption = s2
I.Enabled = False
ElseIf (ic = 5) And (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
ElseIf (ic = 6) And (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (ic = 7) And (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (ic = 8) And (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (C.Enabled = True) Then
C.Caption = s2
C.Enabled = False
ElseIf (B.Enabled = True) Then
B.Caption = s2
B.Enabled = False
ElseIf (D.Enabled = True) Then
D.Caption = s2
D.Enabled = False
ElseIf (E.Enabled = True) Then
E.Caption = s2
E.Enabled = False
ElseIf (F.Enabled = True) Then
F.Caption = s2
F.Enabled = False
ElseIf (G.Enabled = True) Then
G.Caption = s2
G.Enabled = False
ElseIf (H.Enabled = True) Then
H.Caption = s2
H.Enabled = False
ElseIf (A.Enabled = True) Then
A.Caption = s2
A.Enabled = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s1) Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s1) Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s1) Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s1) Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s1) Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s1) Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s1) Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s1) Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = s2) Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = s2) Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = s2) Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = s2) Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = s2) Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = s2) Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = s2) Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = s2) Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox "Computer Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If

If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
x.Enabled = True
o.Enabled = True
End If
Label3.Caption = n1 + "'s turn."
T9.Enabled = False
End Sub

Private Sub Text1_Change()
n1 = Text1.Text
Command1.Enabled = True
End Sub

Private Sub x_Click()
s1 = "X"
s2 = "O"
A.Enabled = True
B.Enabled = True
C.Enabled = True
D.Enabled = True
E.Enabled = True
F.Enabled = True
G.Enabled = True
H.Enabled = True
I.Enabled = True
o.Enabled = False
End Sub

Private Sub o_Click()
s1 = "O"
s2 = "X"
A.Enabled = True
B.Enabled = True
C.Enabled = True
D.Enabled = True
E.Enabled = True
F.Enabled = True
G.Enabled = True
H.Enabled = True
I.Enabled = True
x.Enabled = False
A.Caption = s2
A.Enabled = False
Label3.Caption = n1 + "'s turn."
End Sub


