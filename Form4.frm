VERSION 5.00
Begin VB.Form Form4 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "1P vs 2P"
   ClientHeight    =   7800
   ClientLeft      =   2760
   ClientTop       =   1965
   ClientWidth     =   9495
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   18
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   -1  'True
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form4"
   MaxButton       =   0   'False
   ScaleHeight     =   7800
   ScaleWidth      =   9495
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   6840
      TabIndex        =   22
      Top             =   5880
      Width           =   1695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6000
      TabIndex        =   16
      Top             =   1680
      Width           =   1215
   End
   Begin VB.TextBox Text2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4920
      TabIndex        =   12
      Top             =   960
      Width           =   3015
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4920
      TabIndex        =   0
      Top             =   240
      Width           =   3015
   End
   Begin VB.CommandButton A 
      BackColor       =   &H00808080&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   720
      TabIndex        =   10
      Top             =   3840
      Width           =   1095
   End
   Begin VB.CommandButton B 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   2040
      TabIndex        =   9
      Top             =   3840
      Width           =   1095
   End
   Begin VB.CommandButton C 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3360
      TabIndex        =   8
      Top             =   3840
      Width           =   1095
   End
   Begin VB.CommandButton D 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   720
      TabIndex        =   7
      Top             =   4920
      Width           =   1095
   End
   Begin VB.CommandButton E 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   2040
      TabIndex        =   6
      Top             =   4920
      Width           =   1095
   End
   Begin VB.CommandButton F 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3360
      TabIndex        =   5
      Top             =   4920
      Width           =   1095
   End
   Begin VB.CommandButton G 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   720
      TabIndex        =   4
      Top             =   6000
      Width           =   1095
   End
   Begin VB.CommandButton H 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   2040
      TabIndex        =   3
      Top             =   6000
      Width           =   1095
   End
   Begin VB.CommandButton I 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3360
      TabIndex        =   2
      Top             =   6000
      Width           =   1095
   End
   Begin VB.CommandButton exit 
      Caption         =   "Quit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   6840
      TabIndex        =   1
      Top             =   6840
      Width           =   1695
   End
   Begin VB.Label Label10 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "MS Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000040C0&
      Height          =   495
      Left            =   5640
      TabIndex        =   23
      Top             =   2520
      Width           =   3135
   End
   Begin VB.Line Line14 
      BorderWidth     =   3
      X1              =   5160
      X2              =   9240
      Y1              =   4800
      Y2              =   4800
   End
   Begin VB.Label Label9 
      BackColor       =   &H00C0E0FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   375
      Left            =   7800
      TabIndex        =   21
      Top             =   5040
      Width           =   1095
   End
   Begin VB.Label Label8 
      BackColor       =   &H00C0E0FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   375
      Left            =   5640
      TabIndex        =   20
      Top             =   5040
      Width           =   1095
   End
   Begin VB.Line Line13 
      BorderWidth     =   3
      X1              =   7200
      X2              =   7200
      Y1              =   4080
      Y2              =   5520
   End
   Begin VB.Label Label7 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   7440
      TabIndex        =   19
      Top             =   4200
      Width           =   1815
   End
   Begin VB.Label Label6 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   5160
      TabIndex        =   18
      Top             =   4200
      Width           =   1815
   End
   Begin VB.Label Label5 
      Caption         =   "Score board"
      ForeColor       =   &H00000080&
      Height          =   375
      Left            =   5760
      TabIndex        =   17
      Top             =   3360
      Width           =   3015
   End
   Begin VB.Label Label4 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   375
      Left            =   1680
      TabIndex        =   15
      Top             =   2520
      Width           =   2775
   End
   Begin VB.Label Label3 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   1680
      TabIndex        =   14
      Top             =   1920
      Width           =   2775
   End
   Begin VB.Label Label2 
      Caption         =   "Enter second player's name :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   375
      Left            =   1200
      TabIndex        =   13
      Top             =   960
      Width           =   3615
   End
   Begin VB.Label Label1 
      Caption         =   "Enter first player's name :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   375
      Left            =   1200
      TabIndex        =   11
      Top             =   360
      Width           =   3615
   End
   Begin VB.Line Line1 
      BorderWidth     =   5
      X1              =   1920
      X2              =   1920
      Y1              =   3360
      Y2              =   7320
   End
   Begin VB.Line Line2 
      BorderWidth     =   5
      X1              =   3240
      X2              =   3240
      Y1              =   3360
      Y2              =   7320
   End
   Begin VB.Line Line3 
      BorderWidth     =   5
      X1              =   360
      X2              =   4920
      Y1              =   4800
      Y2              =   4800
   End
   Begin VB.Line Line4 
      BorderWidth     =   5
      X1              =   360
      X2              =   4920
      Y1              =   5880
      Y2              =   5880
   End
   Begin VB.Line Line5 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   240
      X2              =   4920
      Y1              =   4200
      Y2              =   4200
   End
   Begin VB.Line Line6 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   3840
      X2              =   3840
      Y1              =   3360
      Y2              =   7320
   End
   Begin VB.Line Line7 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   240
      X2              =   4920
      Y1              =   6480
      Y2              =   6480
   End
   Begin VB.Line Line8 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   1200
      X2              =   1200
      Y1              =   3360
      Y2              =   7320
   End
   Begin VB.Line Line9 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   240
      X2              =   4920
      Y1              =   5280
      Y2              =   5280
   End
   Begin VB.Line Line10 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   2520
      X2              =   2520
      Y1              =   3360
      Y2              =   7320
   End
   Begin VB.Line Line11 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   480
      X2              =   4680
      Y1              =   3600
      Y2              =   7080
   End
   Begin VB.Line Line12 
      BorderWidth     =   3
      Visible         =   0   'False
      X1              =   4680
      X2              =   480
      Y1              =   3600
      Y2              =   7080
   End
End
Attribute VB_Name = "Form4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim s1 As String, s2 As String, t As String, n1 As String, n2 As String, m1 As Integer, m2 As Integer
Private Sub A_Click()
A.Caption = s1
A.Enabled = False

If A.Caption = "X" Then
Label10.Caption = n2 + "'s turn."
ElseIf A.Caption = "O" Then
Label10.Caption = n1 + "'s turn."
End If

t = s1 'toggling symbols
s1 = s2
s2 = t

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "X") Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "X") Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "X") Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "X") Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "X") Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "X") Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "X") Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "X") Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "O") Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "O") Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "O") Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "O") Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "O") Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "O") Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "O") Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "O") Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If

If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If
End Sub

Private Sub B_Click()
B.Caption = s1
B.Enabled = False

If B.Caption = "X" Then
Label10.Caption = n2 + "'s turn."
ElseIf B.Caption = "O" Then
Label10.Caption = n1 + "'s turn."
End If

t = s1
s1 = s2
s2 = t


If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "X") Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "X") Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "X") Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "X") Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "X") Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "X") Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "X") Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "X") Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "O") Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "O") Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "O") Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "O") Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "O") Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "O") Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "O") Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "O") Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If


If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If
End Sub

Private Sub C_Click()
C.Caption = s1
C.Enabled = False

If C.Caption = "X" Then
Label10.Caption = n2 + "'s turn."
ElseIf C.Caption = "O" Then
Label10.Caption = n1 + "'s turn."
End If

t = s1
s1 = s2
s2 = t


If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "X") Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "X") Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "X") Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "X") Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "X") Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "X") Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "X") Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "X") Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "O") Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "O") Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "O") Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "O") Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "O") Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "O") Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "O") Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "O") Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If


If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If
End Sub

Private Sub Command1_Click()
If Text1.Text = "" Then
Text1.Text = "Player1"
End If
If Text2.Text = "" Then
Text2.Text = "Player2"
End If
Label3.Caption = Text1.Text + "   -     X"
Label4.Caption = Text2.Text + "   -     O"
Text1.Enabled = False
Text2.Enabled = False
A.Enabled = True
B.Enabled = True
C.Enabled = True
D.Enabled = True
E.Enabled = True
F.Enabled = True
G.Enabled = True
H.Enabled = True
I.Enabled = True
Command2.Enabled = True
Label6.Caption = Text1.Text
Label7.Caption = Text2.Text
Label8.Caption = m1
Label9.Caption = m2
Command1.Enabled = False
End Sub

Private Sub Command2_Click()
m1 = 0
m2 = 0
Label8.Caption = m1
Label9.Caption = m2
End Sub

Private Sub D_Click()
D.Caption = s1
D.Enabled = False

If D.Caption = "X" Then
Label10.Caption = n2 + "'s turn."
ElseIf D.Caption = "O" Then
Label10.Caption = n1 + "'s turn."
End If

t = s1
s1 = s2
s2 = t


If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "X") Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "X") Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "X") Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "X") Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "X") Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "X") Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "X") Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "X") Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "O") Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "O") Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "O") Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "O") Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "O") Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "O") Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "O") Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "O") Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If


If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If
End Sub

Private Sub E_Click()
E.Caption = s1
E.Enabled = False

If E.Caption = "X" Then
Label10.Caption = n2 + "'s turn."
ElseIf E.Caption = "O" Then
Label10.Caption = n1 + "'s turn."
End If

t = s1
s1 = s2
s2 = t


If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "X") Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "X") Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "X") Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "X") Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "X") Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "X") Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "X") Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "X") Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "O") Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "O") Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "O") Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "O") Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "O") Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "O") Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "O") Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "O") Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If


If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If
End Sub

Private Sub exit_Click()
m1 = 0
m2 = 0
Text1.Enabled = True
Text1.Text = ""
Text2.Enabled = True
Text2.Text = ""
Label3.Caption = ""
Label4.Caption = ""
Label6.Caption = ""
Label7.Caption = ""
Label8.Caption = ""
Label9.Caption = ""
Label10.Caption = ""
A.Caption = ""
B.Caption = ""
C.Caption = ""
D.Caption = ""
E.Caption = ""
F.Caption = ""
G.Caption = ""
H.Caption = ""
I.Caption = ""
A.Enabled = False
B.Enabled = False
C.Enabled = False
D.Enabled = False
E.Enabled = False
F.Enabled = False
G.Enabled = False
H.Enabled = False
I.Enabled = False
Command1.Enabled = True
Command2.Enabled = False
Form4.Hide
Form1.Show
End Sub

Private Sub F_Click()
F.Caption = s1
F.Enabled = False

If F.Caption = "X" Then
Label10.Caption = n2 + "'s turn."
ElseIf F.Caption = "O" Then
Label10.Caption = n1 + "'s turn."
End If

t = s1
s1 = s2
s2 = t


If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "X") Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "X") Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "X") Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "X") Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "X") Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "X") Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "X") Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "X") Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "O") Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "O") Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "O") Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "O") Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "O") Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "O") Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "O") Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "O") Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If


If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If
End Sub

Private Sub Form_Load()
m1 = 0
m2 = 0
s1 = "X"
s2 = "O"
A.Enabled = False
B.Enabled = False
C.Enabled = False
D.Enabled = False
E.Enabled = False
F.Enabled = False
G.Enabled = False
H.Enabled = False
I.Enabled = False
Text1.Text = ""
Text2.Text = ""
End Sub

Private Sub G_Click()
G.Caption = s1
G.Enabled = False

If G.Caption = "X" Then
Label10.Caption = n2 + "'s turn."
ElseIf G.Caption = "O" Then
Label10.Caption = n1 + "'s turn."
End If

t = s1
s1 = s2
s2 = t


If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "X") Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "X") Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "X") Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "X") Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "X") Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "X") Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "X") Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "X") Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "O") Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "O") Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "O") Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "O") Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "O") Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "O") Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "O") Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "O") Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If


If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If
End Sub

Private Sub H_Click()
H.Caption = s1
H.Enabled = False

If H.Caption = "X" Then
Label10.Caption = n2 + "'s turn."
ElseIf H.Caption = "O" Then
Label10.Caption = n1 + "'s turn."
End If

t = s1
s1 = s2
s2 = t


If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "X") Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "X") Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "X") Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "X") Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "X") Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "X") Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "X") Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "X") Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "O") Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "O") Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "O") Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "O") Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "O") Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "O") Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "O") Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "O") Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If


If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If
End Sub

Private Sub I_Click()
I.Caption = s1
I.Enabled = False

If I.Caption = "X" Then
Label10.Caption = n2 + "'s turn."
ElseIf I.Caption = "O" Then
Label10.Caption = n1 + "'s turn."
End If

t = s1
s1 = s2
s2 = t


If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "X") Then
Line5.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "X") Then
Line8.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "X") Then
Line11.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "X") Then
Line9.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "X") Then
Line7.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "X") Then
Line10.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "X") Then
Line6.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "X") Then
Line12.Visible = True
m1 = m1 + 1
Label8.Caption = m1
MsgBox n1 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If

If (A.Caption = B.Caption) And (B.Caption = C.Caption) And (C.Caption = "O") Then
Line5.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = D.Caption) And (D.Caption = G.Caption) And (G.Caption = "O") Then
Line8.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (A.Caption = E.Caption) And (E.Caption = I.Caption) And (I.Caption = "O") Then
Line11.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (D.Caption = E.Caption) And (E.Caption = F.Caption) And (F.Caption = "O") Then
Line9.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (G.Caption = H.Caption) And (H.Caption = I.Caption) And (I.Caption = "O") Then
Line7.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (B.Caption = E.Caption) And (E.Caption = H.Caption) And (H.Caption = "O") Then
Line10.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = F.Caption) And (F.Caption = I.Caption) And (I.Caption = "O") Then
Line6.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
ElseIf (C.Caption = E.Caption) And (E.Caption = G.Caption) And (G.Caption = "O") Then
Line12.Visible = True
m2 = m2 + 1
Label9.Caption = m2
MsgBox n2 + " Wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If


If (A.Enabled = False) And (B.Enabled = False) And (C.Enabled = False) And (D.Enabled = False) And (E.Enabled = False) And (F.Enabled = False) And (G.Enabled = False) And (H.Enabled = False) And (I.Enabled = False) Then
MsgBox " Nobody wins!"
A.Caption = ""
A.Enabled = True
B.Caption = ""
B.Enabled = True
C.Caption = ""
C.Enabled = True
D.Caption = ""
D.Enabled = True
E.Caption = ""
E.Enabled = True
F.Caption = ""
F.Enabled = True
G.Caption = ""
G.Enabled = True
H.Caption = ""
H.Enabled = True
I.Caption = ""
I.Enabled = True
Line5.Visible = False
Line6.Visible = False
Line7.Visible = False
Line8.Visible = False
Line9.Visible = False
Line10.Visible = False
Line11.Visible = False
Line12.Visible = False
End If
End Sub

Private Sub Text1_Change()
n1 = Text1.Text
End Sub

Private Sub Text2_Change()
n2 = Text2.Text
Command1.Enabled = True
End Sub
