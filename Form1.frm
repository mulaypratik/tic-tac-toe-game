VERSION 5.00
Object = "{C1A8AF28-1257-101B-8FB0-0020AF039CA3}#1.1#0"; "MCI32.OCX"
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Tic-Tac-Toe"
   ClientHeight    =   6000
   ClientLeft      =   1995
   ClientTop       =   2355
   ClientWidth     =   9960
   ForeColor       =   &H00000000&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   6000
   ScaleWidth      =   9960
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command3 
      Caption         =   "About..."
      BeginProperty Font 
         Name            =   "Perpetua"
         Size            =   21.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   3720
      TabIndex        =   7
      Top             =   5160
      Width           =   2415
   End
   Begin MCI.MMControl MMControl2 
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   5160
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   661
      _Version        =   393216
      DeviceType      =   ""
      FileName        =   "D:\My Projects\Tic-Tac-Toe\Click.wav"
   End
   Begin MCI.MMControl MMControl1 
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   5520
      Visible         =   0   'False
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   661
      _Version        =   393216
      DeviceType      =   ""
      FileName        =   "D:\My Projects\Tic-Tac-Toe\Sound.mp3"
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Quit"
      BeginProperty Font 
         Name            =   "Perpetua"
         Size            =   21.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   3720
      TabIndex        =   2
      Top             =   4200
      Width           =   2415
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H8000000A&
      Caption         =   "Play Now!"
      BeginProperty Font 
         Name            =   "Perpetua"
         Size            =   27.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   3720
      Picture         =   "Form1.frx":0000
      TabIndex        =   0
      Top             =   3240
      Width           =   2415
   End
   Begin VB.Label Label4 
      BackColor       =   &H00C0FFFF&
      Caption         =   "beta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   8400
      TabIndex        =   8
      Top             =   720
      Width           =   615
   End
   Begin VB.Label Label3 
      Caption         =   " - Pratik Mulay."
      BeginProperty Font 
         Name            =   "MS Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   375
      Left            =   7560
      TabIndex        =   4
      Top             =   5520
      Width           =   2055
   End
   Begin VB.Label Label2 
      BackColor       =   &H008080FF&
      Caption         =   " - with the concept of Artificial Intelligence."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   375
      Left            =   4080
      TabIndex        =   3
      Top             =   2160
      Width           =   5535
   End
   Begin VB.Image Image3 
      Height          =   1905
      Left            =   360
      Picture         =   "Form1.frx":1472
      Top             =   3240
      Width           =   1905
   End
   Begin VB.Image Image2 
      Height          =   2025
      Left            =   7320
      Picture         =   "Form1.frx":28F5
      Top             =   3000
      Width           =   2025
   End
   Begin VB.Label Label1 
      BackColor       =   &H00C0E0FF&
      Caption         =   "Tic - Tac - Toe"
      BeginProperty Font 
         Name            =   "Palatino Linotype"
         Size            =   48
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1455
      Left            =   2160
      TabIndex        =   1
      Top             =   720
      Width           =   6255
   End
   Begin VB.Image Image1 
      Height          =   1785
      Left            =   360
      Picture         =   "Form1.frx":3E08
      Top             =   600
      Width           =   1575
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Form1.Hide
Form5.Show
MMControl2.Command = "Open"
MMControl2.Command = "Play"
End Sub

Private Sub Command2_Click()
Form1.Hide
Form3.Show
MMControl2.Command = "Open"
MMControl2.Command = "Play"
End Sub


Private Sub Command3_Click()
Form1.Hide
Form6.Show
End Sub

Private Sub Form_Load()
MMControl1.Command = "Open"
MMControl1.Command = "Play"
End Sub
