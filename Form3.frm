VERSION 5.00
Begin VB.Form Form3 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Exit"
   ClientHeight    =   5145
   ClientLeft      =   3930
   ClientTop       =   2925
   ClientWidth     =   6600
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   ScaleHeight     =   5145
   ScaleWidth      =   6600
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Caption         =   "No"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   3840
      TabIndex        =   0
      Top             =   1680
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Yes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1320
      TabIndex        =   2
      Top             =   1680
      Width           =   1455
   End
   Begin VB.Image Image2 
      Height          =   2010
      Left            =   720
      Picture         =   "Form3.frx":0000
      Top             =   2760
      Width           =   2025
   End
   Begin VB.Image Image1 
      Height          =   2025
      Left            =   3840
      Picture         =   "Form3.frx":1013
      Top             =   2760
      Width           =   2025
   End
   Begin VB.Label Label1 
      Caption         =   "Do you want to really Quit?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   735
      Left            =   240
      TabIndex        =   1
      Top             =   480
      Width           =   6255
   End
End
Attribute VB_Name = "Form3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
End
End Sub

Private Sub Command2_Click()
Form3.Hide
Form1.Show
End Sub

